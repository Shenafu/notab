#!/usr/bin/lua5.3

--[[
!!AB , aka NOTAB, programming language

--]]

-- lexer
function lexer()
	local lex = {}
	local tok = ''
	state_string = false
	while true do

		local char = io.read(1)

		if not char then break end

		tok = tok .. char

		if char == '"' and state_string then
			state_string = false
			table.insert(lex, 'STR=' .. tok:sub(2, -2))
			tok = ''
		elseif char == '"' then
			state_string = true
		elseif state_string then
		elseif tok == '$<' then
			table.insert(lex, 'PRINT')
			tok = ''
		elseif tok == '$>' then
			table.insert(lex, 'INPUT')
			tok = ''
		elseif tok == ',' then
			table.insert(lex, 'COMMA')
			tok = ''
		elseif char == ' ' or char == '\n' then
			tok = ''
		end
	end

	return lex
end


-- parser
function parser(lex)
	local i = 1
	while (lex[i]) do
		if lex[i] == 'PRINT' then
			local r = ''
			i = i + 1
			while (lex[i] and lex[i]:sub(1, 3) == 'STR') do
				r = r .. lex[i]:sub(5)
				i = i + 1
				if not lex[i] or lex[i] ~= 'COMMA' then break end
				i = i + 1
			end
			print(r)
		else
			i = i + 1
		end
	end
end

-- read file from commandline
myfile= arg[1]
myscript = io.input(myfile, "r")

parser(lexer())

myscript:close()


