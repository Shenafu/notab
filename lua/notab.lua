#!/usr/bin/lua5.1

local rex = require "rex_pcre"

--[[
!!AB , aka NOTAB, programming language

--]]

-- lexer
function lexer(myscript)
	local lex = {}
	local tok = ''
	state_string = false
	local script = myscript and myscript:read("*a") or io.read()

	while #script > 0 do
		if state_string and rex.find(script, '^"') then
			state_string = false
			script = script:sub(2)
		elseif state_string and rex.find(script, '^((?<=\\\\)"|[^"\r\n])*') then
			local a,b = rex.find(script, '^((?<=\\\\)"|[^"\r\n])*')
			local s = rex.gsub(script:sub(a,b), '\\\\"', '"')
			table.insert(lex, 'STR=' .. s)
			script = script:sub(b+1)
		elseif rex.find(script, '^"') then
			state_string = true
			script = script:sub(2)
		elseif rex.find(script, '^[0-9]?[a-zA-Z0-9]*[a-zA-Z][a-zA-Z0-9]*') then
			local a,b = rex.find(script, '^[0-9]?[a-zA-Z0-9]*[a-zA-Z][a-zA-Z0-9]*')
			table.insert(lex, 'VAR=' .. script:sub(a,b))
			script = script:sub(b+1)
		elseif rex.find(script, '^[0-9]+[.][0-9]*') then
			local a,b = rex.find(script, '^[0-9]+[.][0-9]*')
			table.insert(lex, 'FLT=' .. script:sub(a,b))
			script = script:sub(b+1)
		elseif rex.find(script, '^[0-9]+') then
			local a,b = rex.find(script, '^[0-9]+')
			table.insert(lex, 'INT=' .. script:sub(a,b))
			script = script:sub(b+1)
		elseif rex.find(script, '^[$]<') then
			table.insert(lex, 'PRINT')
			script = script:sub(3)
		elseif rex.find(script, '^[$]>') then
			table.insert(lex, 'INPUT')
			script = script:sub(3)
		elseif rex.find(script, '^[+]') then
			table.insert(lex, 'PLUS')
			script = script:sub(2)
		elseif rex.find(script, '^,') then
			table.insert(lex, 'COMMA')
			script = script:sub(2)
		elseif rex.find(script, '^[ \t\r\n]') then
			script = script:sub(2)
		elseif rex.find(script, '^.') then
			script = script:sub(2)
		end
		print(lex[#lex])
	end

	return lex
end


-- parser
function parser(lex)
	print()
	print('=====================')
	local i = 1
	while (lex[i]) do
		--print("LEX=" .. lex[i])
		if lex[i] == 'PRINT' then
			local r = ''
			i = i + 1
			while (lex[i]) do
				local s = lex[i]
				local stype = s:sub(1, 4)
				if (stype == 'STR=' or stype == 'INT=' or stype == 'FLT=' ) then
					r = r .. s:sub(5)
					i = i + 1
					if not lex[i] or lex[i] ~= 'PLUS' then break end
				end
				i = i + 1 -- is COMMA, so continue printing
			end
			print(r)
		else
			i = i + 1
		end
	end
	print('=====================')
end

-- read file from commandline
myfile= arg[1]
myscript = io.input(myfile, "r")

parser(lexer(myscript))

myscript:close()


