%{
#include <iostream>
#include "notab.h"
#include "ast.h"
#include "y.tab.h"  // to get the token types from Bison

using namespace std;
extern int line_num;
extern char buf[MAX_STR_CONST];
extern char *s;

%}

DIGIT [0-9]
LETTER [a-zA-Z]

%x MODE_STRING
%x MODE_COMMENT
%%

 /* STRING code from https://www.cs.virginia.edu/~cr4bd/flex-manual/Start-Conditions.html */

\"           { s = buf; BEGIN(MODE_STRING); }
<MODE_STRING>{
\"           { /* saw closing quote - all done */
		BEGIN(INITIAL);
		*s = '\0';
		yylval.sval = strdup(buf);
		return STRING;
	}
\n        {
/* error - unterminated string constant */
/* generate error message */
/* allow for now */
}

\\[0-7]{1,3} {
/* octal escape sequence */
int result;

(void) sscanf( strdup(yytext) + 1, "%o", &result );

if ( result > 0xff )
/* error, constant is out-of-bounds */

*s++ = result;
}

\\[0-9]+ {
/* generate error - bad escape sequence; something
* like '\48' or '\0777777'
*/
}

\\n  *s++ = '\n';
\\t  *s++ = '\t';
\\r  *s++ = '\r';
\\b  *s++ = '\b';
\\f  *s++ = '\f';

\\(.|\n)  *s++ = strdup(yytext)[1];

[^\\\n\"]+   {
		char *yptr = strdup(yytext);
		while ( *yptr ) {
			*s++ = *yptr++;
		}
	}
}

"'["		       { BEGIN(MODE_COMMENT); }
<MODE_COMMENT>{
	[^'\n]*      // ignore anything that's not a '
	'+[^'\]\n]*  // ignore ' not followed by ]
	\n           { ++line_num; }
	'+"]"        { BEGIN(INITIAL); }
}

'[^\[][^\n]*		; // comment, ignore rest of line

{DIGIT}*[a-zA-Z0-9]*{LETTER}[a-zA-Z0-9]* { // We have to strdup because we can't rely on yytext not changing underneath us:
	yylval.id = strdup(yytext); return ID; }
{DIGIT}+"."?{DIGIT}*  { yylval.fval = atof(strdup(yytext)); return FLOAT; }

"$<"         { return OUTPUT; }
"$>"         { return INPUT; }
"$"         { return '$'; }
"#"         { return '#'; }
\\\n   ; // allow backslash \ to resume statement on next line
"="      { return '='; }
","      { return ','; }
"("    { return '('; }
")"    { return ')'; }
"+"    { return '+'; }
"-"    { return '-'; }
"*"    { return '*'; }
"/"    { return '/'; }

";"    { return ';'; }
\n     { ++line_num; return ENDL; }
<<EOF>>     { static int once = 0; return (once = !once) ? ENDL : 0; }
[ \t]  ;
.
%%
