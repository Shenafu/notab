%{
#include <iostream>
#include <string.h>
#include <stdlib.h>
#include <map>
#include <list>
#include "ast.h"

using namespace std;

// the root of the abstract syntax tree
pgm *root;

int YYMAXDEPTH = 65536;

// declare stuff from flex that bison needs to know about
extern int yylex();
extern int yyparse();
extern FILE *yyin;
extern int line_num;

void yyerror(const char *s);
%}

// Bison fundamentally works by asking flex to get the next token,
// which it returns as an object of type "yystype".  But tokens could // be of any arbitrary data type!  So we deal with that in Bison by
// defining a C union holding each of the types of tokens that Flex
// could return, and have Bison use that union instead of "int" for
// the definition of "yystype":
%union {
	float fval;
	char *sval;
	char *id;
	exp_node *expnode;
	list<statement *> *stmts;
	statement *st;
	pgm *prog;
}

// define the constant-string tokens:
%token OUTPUT
%token INPUT
%token ENDL ';'
%token '='
%token '$' '#'
%token '(' ')'
%token '+' '-' '*' '/'

// Define the "terminal symbol" token types I'm going to use (in CAPS
// by convention), and associate each with a field of the union:
%token <fval> FLOAT
%token <id> ID
%token <sval> STRING
%type <sval> strings
%type <expnode> exp
%type <stmts> stmtlist
%type <st> stmt
%type <prog> program
%type <st> assign
%type <st> do_output


%right '='
%left '+' '-'
%left '*' '/'
%left '(' ')'

%start program
%%
program:
	stmtlist { $$ = new pgm($1); root = $$; }
	;
stmtlist:
	stmtlist endstmts	{ $$ = $1; }
	|
	stmtlist stmt endstmts	{ // copy up the list and add the stmt to it
		$$ = $1;
		$1->push_back($2);
	}
	|
	{ $$ = new list<statement *>(); }  /* empty string */
	;
endstmts:
	endstmts endstmt
	|
	endstmt
	;
endstmt:
	ENDL
	|
	';'
	;
stmt:
	do_output
	|
	assign
	;
assign:
	'#' ID '=' exp		{ $$ = new assignment_stmt($2, $4); }
	;
exp:
	exp '+' exp	{ $$ = new plus_node($1, $3); }
	|
	exp '-' exp	{ $$ = new minus_node($1, $3); }
	|
	exp '*' exp	{ $$ = new times_node($1, $3); }
	|
	exp '/' exp	{ $$ = new divide_node($1, $3); }
	|
	'(' exp ')'	{ $$ = $2; }
	|
	'-' exp		{ $$ = new unary_minus_node($2); }
	|
	ID				{ $$ = new id_node($1); }
	|
	FLOAT			{ $$ = new number_node($1); }
	;
do_output:
	OUTPUT strings  {cout << $2; }
	|
	OUTPUT { cout << "\n"; }
	;
strings:
	strings '+' strings { $$ = strcat($1, $3); }
	|
	STRING
	|
	FLOAT { char s[32]; sprintf(s, "%g", $1); $$ = s; }
	;
%%
int main(int, char**) {
	// Open a file handle to a particular file:
	// Parse through the input:
	yyparse();

}

void yyerror(const char *s) {
	cout << "EEK, parse error on line " << line_num << "!  Message: " << s << endl;
	// might as well halt now:
	//exit(-1);
}
